package wrapper

/*
#cgo pkg-config –define-variable=prefix=.: txml_Wrapper
#cgo CXXFLAGS: -std=c++1z
#include "txml_Wrapper.h"
#include <stdbool.h>
#include <stdlib.h>

bool GoCallbackEx_cgo(char*, void*);
*/
import "C"
import (
	"log"
	"sync"
	"unsafe"
)

type Wrapper struct {
	id            string
	cbCh          chan string
	sendLock      *sync.RWMutex
	isInitialized bool
}

// NewWrapper creates a new Wrapper
func New(id string) *Wrapper {
	return &Wrapper{
		id:            id,
		cbCh:          make(chan string, 10),
		sendLock:      &sync.RWMutex{},
		isInitialized: false,
	}
}

// LoadLib loads new instance of txml connector dll according to passed id
func LoadLib(id, libPath string) int {
	cid := C.CString(id)
	cLibPath := C.CString(libPath)
	defer C.free(unsafe.Pointer(cLibPath))
	return int(C.LoadLib(cid, cLibPath))
}

// FreeLib free instance of txml connector dll according to passed id
func FreeLib(id string) int {
	cid := C.CString(id)
	defer C.free(unsafe.Pointer(cid))
	return int(C.FreeLib(cid))
}

// Id gets id passed in when the Wrapper was created
func (w *Wrapper) Id() string {
	return w.id
}

// SetCallbackEx set callback to get async calls from txml connector similarly to setCallback()
// but with user data. In case of current Wrapper it's Wrapper id.
func (w *Wrapper) SetCallbackEx(userData string, handler func(message string)) bool {
	// Handle message go-routine
	go func() {
		for m := range w.cbCh {
			handler(m)
		}
	}()

	cUserData := C.CString(userData)
	cid := C.CString(w.id)
	defer C.free(unsafe.Pointer(cid))

	return bool(C.SetCallbackEx(cid, (C.TCallbackEx)(unsafe.Pointer(C.GoCallbackEx_cgo)), unsafe.Pointer(cUserData)))
}

// Initialize initializes loaded instance of the txml connector DLL.
// Should be done before any calls to connector.
func (w *Wrapper) Initialize(logPath string, logLevel int) string {
	cid := C.CString(w.id)
	clogPath := C.CString(logPath)
	defer C.free(unsafe.Pointer(cid))
	defer C.free(unsafe.Pointer(clogPath))

	result := C.Initialize(cid, clogPath, C.int(logLevel))
	defer w.FreeMemory(result)

	return C.GoString(result)
}

// Uninitialize loaded instance of the txml connector DLL.
func (w *Wrapper) UnInitialize() string {
	cid := C.CString(w.id)
	defer C.free(unsafe.Pointer(cid))

	result := C.UnInitialize(cid)
	defer w.FreeMemory(result)

	return C.GoString(result)
}

// SendCommand sends command to the connector to get command callback in async way in callback.
// See setCallback and setCallbackEx methods. Passed data string should have xml format.
func (w *Wrapper) SendCommand(pData string) string {
	cid := C.CString(w.id)
	cpData := C.CString(pData)
	defer C.free(unsafe.Pointer(cid))
	defer C.free(unsafe.Pointer(cpData))

	result := C.SendCommand(cid, cpData)
	defer w.FreeMemory(result)

	return C.GoString(result)
}

// SetLogLevel set log level for the txml connector lib.
func (w *Wrapper) SetLogLevel(logLevel int) string {
	cid := C.CString(w.id)
	defer C.free(unsafe.Pointer(cid))

	result := C.SetLogLevel(cid, C.int(logLevel))
	defer w.FreeMemory(result)

	return C.GoString(result)
}

// FreeMemory free memory that were allocated by txml connector lib.
// Should be used by the setCallback and setCallbackEx methods to free memory holden
// by data passed in ones.
func (w *Wrapper) FreeMemory(data *C.char) bool {
	if data == nil {
		return true
	}
	cid := C.CString(w.id)
	defer C.free(unsafe.Pointer(cid))

	return bool(C.FreeMemory(cid, data))
}

// GoCallbackEx callback to pass data and user data (Wrapper id) from the txml connector
// One Wrapper <-> one thread, so callback should not be blocked for a long time
//export GoCallbackEx
func GoCallbackEx(data *C.char, userData unsafe.Pointer) bool {
	id := C.GoString((*C.char)(userData))

	w, ok := Wrappers[id]
	if !ok {
		log.Printf("Cannot get Wrapper by id \"%v\"to handle callback "+
			"message from connector/n.", id)
		return false
	}

	w.cbCh <- C.GoString(data)
	return w.FreeMemory(data)
}
