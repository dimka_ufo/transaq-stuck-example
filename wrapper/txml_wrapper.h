#include <stdbool.h>

#ifndef txml_wrapper
#define txml_wrapper

#ifdef __cplusplus
extern "C" {
#endif
    typedef bool (*TCallback)(char*);
    typedef bool (*TCallbackEx)(char*, void*);

    int LoadLib(char* id, char* libPath);
    int FreeLib(char* id);

    char* Initialize(char* id, char* logPath, int logLevel);
    char* UnInitialize(char* id );

    bool SetCallback(char* id, TCallback pCallback);
    bool SetCallbackEx(char* id, TCallbackEx pCallbackEx, void* userData);
    char* SendCommand(char* id, char* pData);
    char* SetLogLevel(char* id, int logLevel);

    bool FreeMemory(char* id, char* pData);
#ifdef __cplusplus
}
#endif

#endif  // txml_wrapper