#include <windows.h>
#include <unordered_map>
#include <shared_mutex>
#include <mutex>
#include <functional>
#include <iterator>
#include <string>
#include "wrapper.hpp"

using namespace std;

template <class Tp>
struct wrapper_equals_to
{
    bool operator()(const Tp &x, const Tp &y) const
    {
        return strcmp(x, y) == 0;
    }
};

struct wrapper_hasher
{
    //BKDR hash algorithm
    int operator()(char* str)const
    {
        int seed = 131;
        int hash = 0;
        while(*str)
        {
            hash = (hash * seed) + (*str);
            str++;
        }

        return hash & (0x7FFFFFFF);
    }
};

typedef unordered_map<char*, Wrapper*, wrapper_hasher, wrapper_equals_to<char*> > wrappers_map;

wrappers_map wrappers;
shared_mutex mutex_;

void* doAction(char* id, const function <void*(Wrapper*)> f) {
    shared_lock<shared_mutex> lock(mutex_);

    wrappers_map::iterator w = wrappers.find(id);
    if (w != wrappers.end())
    {
         return f(w->second);
    }

    return NULL;
}

int LoadLib(char* id, char* libPath) {
    unique_lock<shared_mutex> lock(mutex_);

    wrappers_map::iterator w = wrappers.find(id);
    if (w == wrappers.end())
    {
        HMODULE hDll = LoadLibrary(libPath);
        if(NULL == hDll)
        {
            return EXIT_FAILURE;
        }

        wrappers[id] = new Wrapper(id, hDll);
    }

    return EXIT_SUCCESS;
}

char* Initialize(char* id, char* logPath, int logLevel) {
    return (char*)doAction(
        id,
        [logPath, logLevel](auto w) -> char* {
            return w->initialize(logPath, logLevel);
        }
    );
}

char* UnInitialize(char* id) {
    return (char*)doAction(
        id,
        [](auto w) -> char* {
            return w->unInitialize();
        }
    );
}

bool SetCallback(char* id, TCallback pCallback) {
    return *(bool*)doAction(
        id,
        [pCallback](auto w) -> bool* {
            return new bool(w->setCallback(pCallback));
        }
    );
}

bool SetCallbackEx(char* id, TCallbackEx pCallbackEx, void* userData) {
    return *(bool*)doAction(
        id,
        [pCallbackEx, userData](auto w) -> bool* {
            return new bool(w->setCallbackEx(pCallbackEx, userData));
        }
    );
}

char* SendCommand(char* id, char* pData) {
    return (char*)doAction(
        id,
        [pData](auto w) -> char* {
            return w->sendCommand(pData);
        }
    );
}

char* SetLogLevel(char* id, int logLevel) {
    return (char*)doAction(
        id,
        [logLevel](auto w) -> char* {
            return w->setLogLevel(logLevel);
        }
    );
}

bool FreeMemory(char* id, char* pData) {
    return *(bool*)doAction(
        id,
        [pData](auto w) -> bool* {
            return new bool(w->freeMemory(pData));
        }
    );
}

int FreeLib(char* id) {
    unique_lock<shared_mutex> lock(mutex_);

    wrappers_map::iterator w = wrappers.find(id);
    if (w != wrappers.end())
    {
        HMODULE hDll = w->second->getHModule();
        wrappers.erase(id);
        return FreeLibrary(hDll);
    }

    return EXIT_FAILURE;
}