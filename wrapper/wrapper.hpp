#include <windows.h>
#include <stdio.h>
#include "txml_wrapper.h"

using namespace std;

#ifndef wrapper
#define wrapper

typedef char* (*InitializeFnPtr)(char*, int);
typedef char* (*UnInitializeFnPtr)();
typedef bool (*SetCallbackFnPtr)(TCallback);
typedef bool (*SetCallbackExFnPtr)(TCallbackEx, void*);
typedef char* (*SendCommandFnPtr)(char*);
typedef char* (*SetLogLevelFnPtr)(int);
typedef bool (*FreeMemoryFnPtr)(char*);

class Wrapper {
public:
    Wrapper(char* id, HMODULE hDll);
    ~Wrapper(){};

    char* initialize(char* logPath, int logLevel);
    char* unInitialize();

    bool setCallback(TCallback pCallback);
    bool setCallbackEx(TCallbackEx pCallbackEx, void* userData);
    char* sendCommand(char* pData);
    char* setLogLevel(int logLevel);

    bool freeMemory(char* pData);

    char* getId();
    HMODULE getHModule();

private:
    mutex mtx;
    char* id;
    HMODULE hDll;
    InitializeFnPtr initializeFn;
    UnInitializeFnPtr unInitializeFn;
    SetCallbackFnPtr setCallbackFn;
    SetCallbackExFnPtr setCallbackExFn;
    SendCommandFnPtr sendCommandFn;
    SetLogLevelFnPtr setLogLevelFn;
    FreeMemoryFnPtr freeMemoryFn;
};

#endif // wrapper