#include <mutex>
#include "wrapper.hpp"

using namespace std;

Wrapper::Wrapper(char* id, HMODULE hDll): id(id), hDll(hDll) {
    initializeFn = (InitializeFnPtr)GetProcAddress(hDll, "Initialize");
    unInitializeFn = (UnInitializeFnPtr)GetProcAddress(hDll, "UnInitialize");
    setCallbackFn = (SetCallbackFnPtr)GetProcAddress(hDll, "SetCallback");
    setCallbackExFn = (SetCallbackExFnPtr)GetProcAddress(hDll, "SetCallbackEx");
    sendCommandFn = (SendCommandFnPtr)GetProcAddress(hDll, "SendCommand");
    setLogLevelFn = (SetLogLevelFnPtr)GetProcAddress(hDll, "SetLogLevel");
    freeMemoryFn = (FreeMemoryFnPtr)GetProcAddress(hDll, "FreeMemory");
}

char* Wrapper::initialize(char* logPath, int logLevel) {
    lock_guard<mutex> lock(mtx);
    return initializeFn(logPath, logLevel);
}

char* Wrapper::unInitialize() {
    lock_guard<mutex> lock(mtx);
    return unInitializeFn();
}

bool Wrapper::setCallback(TCallback pCallback) {
    lock_guard<mutex> lock(mtx);
    return setCallbackFn(pCallback);
}

bool Wrapper::setCallbackEx(TCallbackEx pCallbackEx, void* userData) {
    lock_guard<mutex> lock(mtx);
    return setCallbackExFn(pCallbackEx, userData);
}

char* Wrapper::sendCommand(char* pData) {
    lock_guard<mutex> lock(mtx);
    return sendCommandFn(pData);
}

char* Wrapper::setLogLevel(int logLevel) {
    lock_guard<mutex> lock(mtx);
    return setLogLevelFn(logLevel);
}

bool Wrapper::freeMemory(char* pData) {
    lock_guard<mutex> lock(mtx);
    return freeMemoryFn(pData);
}

char* Wrapper::getId() {
    return id;
}

HMODULE Wrapper::getHModule() {
    return hDll;
}