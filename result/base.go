package result

import (
	"encoding/xml"
	"fmt"
	"log"
	"reflect"
)

type Error struct {
	XMLName xml.Name `xml:"error"`
	Content string   `xml:",chardata"`
}

type Callback struct {
	XMLName      xml.Name `xml:"callback"`
	Error        *Error
	ServerStatus *ServerStatus
}

type Result struct {
	XMLName xml.Name `xml:"result"`
	Success bool     `xml:"success,attr"`
	Message string   `xml:"message"`
}

type Handler interface {
	HandleCallback(hs *Handlers)
}

type Handlers struct {
	Error        func(msg *Error)
	ServerStatus func(msg *ServerStatus)
}

var cbHandlerType = reflect.TypeOf((*Handler)(nil)).Elem()

func Handle(message string, handlers *Handlers) {
	var cb Callback
	cbXml := fmt.Sprintf("<callback>%v</callback>", message)
	if err := Unmarshal([]byte(cbXml), &cb); err != nil {
		log.Printf("Cannot unmarshal callback message:\n%v", message)
		return
	}
	if cb.Error != nil {
		log.Printf("Error from callback: %v\n", cb.Error.Content)
		return
	}

	// Handle possible callback messages
	cbValue := reflect.ValueOf(cb)
	for i := 0; i < cbValue.NumField(); i++ {
		field := cbValue.Field(i)
		if field.Kind() != reflect.Struct &&
			!field.IsNil() &&
			field.CanInterface() &&
			field.Type().Implements(cbHandlerType) {
			field.Interface().(Handler).HandleCallback(handlers)
		}
	}
}
