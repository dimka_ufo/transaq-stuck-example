package result

import (
	"encoding/xml"
)

type ServerStatus struct {
	XMLName  xml.Name `xml:"server_status"`
	Timezone string   `xml:"server_tz,attr"`
	Id       int      `xml:"id,attr"`
	Recover  bool     `xml:"recover,attr,omitempty"`
	Status   string   `xml:"connected,attr"`
	Content  string   `xml:",chardata"`
}

func (s ServerStatus) HandleCallback(hs *Handlers) {
	hs.ServerStatus(&s)
}
