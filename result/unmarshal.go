package result

import (
	"bytes"
	"encoding/xml"
	"errors"
	"io"
)

func getToken(decoder *xml.Decoder) (token xml.Token, err error) {
	if t, e := decoder.Token(); e != nil && e != io.EOF {
		err = e
	} else {
		token = t
	}
	return
}

func Unmarshal(data []byte, obj interface{}) error {
	decoder := xml.NewDecoder(bytes.NewReader(data))
	t, err := getToken(decoder)
	if err != nil {
		return err
	}

	var logicErr Error
	switch se := t.(type) {
	case xml.StartElement:
		if se.Name.Local == "error" {
			err := decoder.DecodeElement(&logicErr, &se)
			if err != nil {
				return err
			}
			return errors.New(logicErr.Content)
		} else if se.Name.Local == "result" {
			err := decoder.DecodeElement(&obj, &se)
			if err != nil {
				return err
			}
			res := obj.(*Result)
			if !res.Success {
				return errors.New(res.Message)
			}
		} else {
			err := decoder.DecodeElement(&obj, &se)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
