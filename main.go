package main

import (
	"context"
	"encoding/xml"
	"flag"
	"fmt"
	"log"
	"strconv"

	"bitbucket.org/dimka_ufo/transaq-stuck-example/command"
	"bitbucket.org/dimka_ufo/transaq-stuck-example/result"
	"bitbucket.org/dimka_ufo/transaq-stuck-example/wrapper"
)

var (
	libFilePath string
	logFilePath string
	logLevel    int
	host        string
	port        int
	login       string
	password    string
)

func init() {
	flag.StringVar(&libFilePath, "lib", "./lib/2.21.2/txmlconnector64.dll", "Path to Transaq connector DLL file")
	flag.StringVar(&logFilePath, "logs", "./logs", "Path to transaq logs")
	flag.IntVar(&logLevel, "logLevel", 3, "Transaq connector log level")
	flag.StringVar(&host, "host", "tr1.finam.ru", "Broker host")
	flag.IntVar(&port, "port", 3900, "Broker port")
	flag.StringVar(&login, "login", "", "Login to connect")
	flag.StringVar(&password, "pass", "", "Pass to connect")
}

func main() {
	flag.Parse()
	log.Printf(fmt.Sprintf("Transaq connector path: %v", libFilePath))

	// Create connector wrapper
	libId := "test-lib-wrapper"
	w := wrapper.New(libId)

	// Put wrapper for callback to wrappers map
	wrapper.Wrappers[libId] = w

	// Load connector lib
	code := wrapper.LoadLib(w.Id(), libFilePath)
	if code != 0 {
		log.Panicln("Cannot load transaq connector dll.")
	}

	// Init connector lib instance
	var initMsg result.Error
	log.Print("Initialize")
	if err := result.Unmarshal([]byte(w.Initialize(logFilePath, logLevel)), &initMsg); err != nil {
		log.Panicf("Cannot initialize transaq connector. Cause: %v\n", err)
	}
	log.Print("Initialized")

	connectCh := make(chan bool)
	isConnected := false
	if login != "" {
		// Set callback handlers
		if !w.SetCallbackEx(w.Id(), func(m string) {
			result.Handle(m, &result.Handlers{
				ServerStatus: func(msg *result.ServerStatus) {
					if msg.Status == "error" {
						log.Printf("Connection error: %v\n", msg.Content)
					} else {
						status, _ := strconv.ParseBool(msg.Status)
						if status {
							isConnected = true
							log.Printf("Connected")
						} else {
							return
						}
					}
					close(connectCh)
				},
			})
		}) {
			log.Panicf("Cannot set callback handlers for wrapper %v\n.", w.Id())
		}

		// Make connect command
		connectCm, err := xml.Marshal(&command.Connect{
			Login:    login,
			Password: password,
			Host:     host,
			Port:     port,
		})
		if err != nil {
			log.Panicf("Cannot marshall connect command. Cause: %v\n", err)
		}

		// Try to connect
		log.Println(fmt.Sprintf("Try to connect to %v:%v", host, port))
		var connectResMessage result.Result
		connectRes := w.SendCommand(string(connectCm))
		if err := result.Unmarshal([]byte(connectRes), &connectResMessage); err != nil {
			log.Panicf("Cannot unmarshall connect result. Cause: %v\n", err)
		}
		if !connectResMessage.Success {
			log.Panicf("Failed to connect. Cause: %v\n", connectResMessage.Message)
		}
	} else {
		log.Print("Login not provided")
		close(connectCh)
	}

	// Listen when connected in separate goroutine to shutdown
	ctx, quit := context.WithCancel(context.Background())
	go func() {
		<-connectCh

		if isConnected {
			// Try to disconnect
			disconnectCm, err := xml.Marshal(&command.Disconnect{})
			if err != nil {
				log.Panicf("Cannot marshall disconnect command. Cause: %v\n", err)
			}
			log.Println(fmt.Sprintf("Try to disconnect from %v:%v", host, port))
			var disconnectResMessage result.Result
			connectRes := w.SendCommand(string(disconnectCm))
			if err := result.Unmarshal([]byte(connectRes), &disconnectResMessage); err != nil {
				log.Panicf("Cannot unmarshall disconnect result. Cause: %v\n", err)
			}
			if !disconnectResMessage.Success {
				log.Panicf("Failed to disconnect. Cause: %v\n", disconnectResMessage.Message)
			}
			log.Println("Disconnected")
		}

		// UnInit connector lib instance
		var unInitMsg result.Error
		log.Print("Uninitialize")
		if err := result.Unmarshal([]byte(w.UnInitialize()), &unInitMsg); err != nil {
			log.Panicf("Cannot uninitialize transaq connector. Cause %v\n", err)
		}
		log.Print("Uninitialized")

		// Unload connector lib
		log.Print("Try to unload transaq connector dll...")
		code = wrapper.FreeLib(w.Id())
		if code == 0 {
			log.Panicln("Cannot unload transaq connector dll.")
		}
		log.Print("Success! Transaq connector dll unload done.")

		quit()
	}()

	<-ctx.Done()
	log.Print("Quit")
}
