package command

import "encoding/xml"

type Base struct {
	XMLName xml.Name `xml:"command"`
	Id      string   `xml:"id,attr"`
}

type Command interface {
	GetId() string
}
