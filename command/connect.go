package command

import "encoding/xml"

type Connect struct {
	Base
	Login          string `xml:"login"`
	Password       string `xml:"password"`
	Host           string `xml:"host"`
	Port           int    `xml:"port"`
	SessionTimeout int    `xml:"session_timeout"`
	RequestTimeout int    `xml:"request_timeout"`
}

func (c Connect) GetId() string {
	return "connect"
}

func (c Connect) MarshalXML(e *xml.Encoder, se xml.StartElement) error {
	type alias Connect
	c.Id = c.GetId()
	return e.Encode((alias)(c))
}

type Disconnect struct {
	Base
}

func (d Disconnect) MarshalXML(e *xml.Encoder, se xml.StartElement) error {
	type alias Disconnect
	d.Id = d.GetId()
	return e.Encode((alias)(d))
}

func (d Disconnect) GetId() string {
	return "disconnect"
}
